all: main.o MyAlgo.o
	gcc -o main main.o MyAlgo.o

main.o: main.c MyAlgo.h
	gcc -c main.c -Wall

MyAlgo.o: MyAlgo.c MyAlgo.h
	gcc -c MyAlgo.c -Wall

clean:
	rm -rf *.o main