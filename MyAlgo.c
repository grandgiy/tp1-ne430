#include <stdio.h>
#include <stdlib.h>
#include "MyAlgo.h"

/* Extrait et retourne STRIDE bits (translatés aux bits de poids les plus faibles) de la valeur
   fournie. L'index correspond au bit de poids le plus faible de la séquence. */
uint8_t extractBits(uint32_t value, int index) {
    return value >> (index - STRIDE) & STRIDE_MASK;
}

/* Crée, initialise un nouveau noeud et le retourne. */
node_t *createNode(uint8_t prefix, uint8_t mask, uint32_t gw) {
    node_t *new = malloc(sizeof(node_t));

    if (new == NULL) {
        printf("Error: could not allocate memory for node.");
        exit(-1);
    }

    new->prefix = prefix;
    new->mask = mask;
    new->gw = gw;
    new->next = NULL;
    new->children = NULL;

    return new;
}

/* Parcourt une liste chaînée triée selon les préfixes jusqu'à trouver la position d'insertion et
   la retourne. */
node_t **getNodeLocation(node_t **head, uint8_t prefix) {
    node_t **location = head, *node = *location;

    /* La liste est triée ; on peut arrêter la recherche dès que le préfixe du noeud est plus grand
       que celui recherché. */
    while (node != NULL && node->prefix < prefix) {
        location = &(node->next);
        node = *location;
    }

    return location;
}

/* Insère un noeud dans une liste chaînée triée selon les préfixes, ou modifie un noeud déjà
   existant s'il y a chevauchement et que le nouveau possède un préfixe plus spécifique. */
void insertNode(node_t **head, uint8_t prefix, uint8_t mask, uint32_t gw) {
    node_t **location = getNodeLocation(head, prefix);
    uint8_t p = prefix;

    // Insère autant de noeuds que nécessaire.
    do {
        node_t *node = *location;

        // Le préfixe ne correspond à aucun noeud existant ; on l'insère.
        if (node == NULL || p != node->prefix) {
            node_t *new = createNode(p, mask, gw);
            new->next = node;

            *location = new;
            location = &(new->next);

        /* Le préfixe correspond à un noeud existant ; on le modifie si son préfixe est moins
           spécifique. Un masque de 0 indique que le noeud ne possède pas de gateway. */
        } else {
            if (mask >= node->mask) {
                node->mask = mask;
                node->gw = gw;
            }

            location = &(node->next);
        }

        p += 1;

    } while ((p & mask) == prefix);
}

/* Supprime récursivement un noeud et tous ses enfants d'un arbre. */
void deleteNode(node_t **tree, uint8_t prefix) {
    node_t **location = getNodeLocation(tree, prefix), *node = *location;

    // Le préfixe correspond à un noeud existant ; on le supprime.
    if (node != NULL && node->prefix == prefix) {
        node_t *child = node->children;
        // Le noeud possède des enfants ; on les supprime également.
        while (child != NULL) deleteNode(&child, child->prefix);

        *location = node->next;
        free(node);
    }
}


// #################################################################################################
// #################################################################################################


/* Fonction « optionnelle » (doit exister mais peut ne rien faire) qui est appelée au lancement du
   programme et peut permettre d'initialiser les éléments nécessaires à l'algo. */
void initMyAlgo() {
    root = createNode(0, 0, 0);
}

/* Fonction qui ajoute une entrée dans la structure de données. L'entrée est supposée valide, i.e.
   addr & netmask == addr. */
void insertMyAlgo(unsigned int addr, unsigned int netmask, unsigned int gw) {
    node_t **head = &(root->children);
    int nextIndex = MAX_BITS - STRIDE;
    uint8_t mask = extractBits(netmask, MAX_BITS), nextMask = extractBits(netmask, nextIndex),
            prefix = extractBits(addr, MAX_BITS);

    // Parcourt l'arbre jusqu'à la bonne hauteur
    while (nextMask != 0) {
        node_t **location = getNodeLocation(head, prefix), *node = *location;

        // Le préfixe recherché ne correspond à aucun noeud existant ; on l'insère.
        if (node == NULL || prefix != node->prefix) {
            // Un masque de 0 indique que le noeud ne possède pas de gateway.
            node_t *new = createNode(prefix, 0, 0);
            new->next = node;

            *location = new;
            node = new;
        }

        head = &(node->children);
        prefix = extractBits(addr, nextIndex);
        nextIndex -= STRIDE;
        mask = nextMask;
        nextMask = extractBits(netmask, nextIndex);
    }

    // Insère le nouveau noeud dans la liste des fils du noeud.
    insertNode(head, prefix, mask, gw);
}

/* Fonction qui recherche une entrée dans la structure de données. */
unsigned int lookupMyAlgo(unsigned int addr) {
    node_t *tree = root;
    int index = MAX_BITS, found = false;
    unsigned int gw = 0;

    /* Descend l'arbre jusqu'à arriver sur une feuille ou un noeud qui ne correspond plus au préfixe
       recherché. */
    while (!found) {
        uint8_t prefix = extractBits(addr, index);
        node_t *node = *getNodeLocation(&(tree->children), prefix);

        /* Le préfixe correspond à un noeud existant ; on enregistre sa gateway. Il peut cependant
           exister des noeuds aux préfixes plus spécifiques, donc on continue la recherche. */
        if (node != NULL && node->prefix == prefix) {
            // Un masque de 0 indique que le noeud ne possède pas de gateway.
            if (node->mask != 0) gw = node->gw;

            tree = node;
            index -= STRIDE;

        } else found = true;
    }

    return gw;
}

/* Fonction qui supprime une entrée dans la structure de données. */
void deleteMyAlgo(unsigned int addr, unsigned int netmask) {
    /* Réflexions :
    Lorsque l'on supprime une entrée, il faut supprimer les noeuds qui correspondent a cette route
    mais qui ne correspondent pas une route moins spécifique. En revanche, il faut remplacer les
    noeuds qui correspondent à une route moins spécifique par cette route.

    Pour ce faire, on pourrait implémenter une structure gateway (qui serait une liste chainée
    de masques et de gateways, triée par ordre décroissant des masques). Les noeuds de l'arbre
    contiendraient donc leur préfixe et un pointeur vers cette liste de gateways.

    Il faudrait donc modifier l'insertion et la recherche en conséquence.
    L'insertion insérerait désormais les gateways de masque plus spécifiques en tête de la liste
    chainée.

    Pour supprimer un noeud, on pourrait donc chercher les noeuds qui correspondent à cette route,
    et supprimer la première gateway de la liste (la plus spécifique).
    Si la liste de gateways devient vide, c'est que le noeud ne correspond plus à aucune route, et
    s'il n'a pas de fils il faut donc le supprimer.

    Si le noeud qu'on vient de supprimer était le seul fils du noeud père, il faut aussi regarder
    si le noeud père possède d'autres routes. Si il n'en possède pas, il faut aussi le supprimmer.
    On devrait ainsi "remonter" l'arbre pour bien supprimer tous les noeuds inutiles.

    Cet ajout augmenterait la complexité spatiale du programme, mais n'aurait que peu d'impact sur
    la vitesse de la recherche. */
}

/* Fonction qui libère toute la mémoire associée à la structure de données. */
void clearMyAlgo() {
    deleteNode(&root, 0);
}
