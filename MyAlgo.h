#include <stdint.h>

#define MAX_BITS 32
// Pas de l'arbre multibit. Doit être strictement inférieur à 8.
#define STRIDE 2
#define STRIDE_MASK ((1 << STRIDE) - 1)
#define true 1
#define false 0

typedef struct node {
    uint8_t prefix, mask;
    uint32_t gw;
    struct node *next, *children;
} node_t;

// Racine de l'arbre.
node_t *root;

/* Fonction « optionnelle » (doit exister mais peut ne rien faire) qui est appelée au lancement du
   programme et peut permettre d'initialiser les éléments nécessaires à l'algo. */
void initMyAlgo();

/* Fonction qui ajoute une entrée dans la structure de données. L'entrée est supposée valide, i.e.
   addr & netmask == addr. */
void insertMyAlgo(unsigned int addr, unsigned int netmask, unsigned int gw);

/* Fonction qui recherche une entrée dans la structure de données. */
unsigned int lookupMyAlgo(unsigned int addr);

/* Fonction qui supprime une entrée dans la structure de données. */
void deleteMyAlgo(unsigned int addr, unsigned int netmask);

/* Fonction qui libère récursivement toute la mémoire associée à la structure de données. */
void clearMyAlgo();
